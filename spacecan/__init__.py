import logging; logger = logging.get_logger(__name__)


try:
    from spacecan_config import config
except ImportError:
    logger.warning(
        "File 'spacecan_config.py' not found. Continuing with defaults.")
    config = {}


"""Constants"""

# filter masks
FULL_MASK = 0x7FF  # can frame id is 11 bits
FUNCTION_MASK = 0x780  # 111 1000 0000
NODE_MASK = 0x07F  # 000 0111 1111

# mapping of CANopen COB-IDs
ID_SYNC = 0x080
ID_HEARTBEAT = 0x700
ID_SCET = 0x180
ID_UTC = 0x200
ID_TC = 0x280
ID_TM = 0x300
ID_MESSAGE = 0x380

# message sending error codes
OK = 1  # successful
ERR_TIMEOUT_FRAME = 2
ERR_TIMEOUT_FLOW_CONTROL = 3
ERR_TIMEOUT_CONSECUTIVE_FRAME = 4
ERR_WRONG_SN = 5
ERR_INVALID_FS = 6  # invalid flow status
ERR_UNEXCPECTED = 7  # an unexpected frame arrived
ERR_WAITFRAME_OVERRUN = 8  # too many wait frames
ERR_BUFFER_OVERFLOW = 9  # flow status 'overflow' received
ERROR = 10  # any other general error


from .bus import Bus
from .frame import Frame
from .heartbeat import HeartbeatProducer, HeartbeatConsumer
from .sync import SyncProducer, SyncConsumer
from .telecommand import TelecommandProducer, TelecommandConsumer
from .telemetry import TelemetryProducer, TelemetryConsumer
from .mailbox import Mailbox
from .message import Message
from .network import Network
from .time import Time, Scet, Utc
from .time import PRECISION_8_BITS, PRECISION_16_BITS, PRECISION_24_BITS
