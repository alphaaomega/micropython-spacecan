from .frame import FrameBuffer
from . import config
from . import (
    ID_SYNC, ID_HEARTBEAT, ID_SCET, ID_UTC, ID_TC, ID_TM,
    ID_MESSAGE, FUNCTION_MASK)


# configuration
FRAME_BUFFER_SIZE = config.get('FRAME_BUFFER_SIZE', 10)


class Network:
    """
    The Network class represents the redundant CAN system bus. It is
    initialized with a node ID and two Bus objects, of which the nominal
    Bus will be the active Bus (until the Bus is switched).

    """
    def __init__(self, node_id, bus_a, bus_b):
        self.node_id = node_id
        self._bus_a = bus_a
        self._bus_b = bus_b
        self._active_bus = self._bus_a
        self._active = False
        self._heartbeat_handler = None
        self._sync_handler = None
        self._scet_handler = None
        self._utc_handler = None
        self._telecommand_handler = None
        self._telemetry_handler = None
        self._message_handler = None
        bus_a.network = self
        bus_b.network = self
        self.frame_buffer = FrameBuffer(FRAME_BUFFER_SIZE)

    def set_filters(self, filters):
        self._bus_a.set_filters(filters)
        self._bus_b.set_filters(filters)

    def get_active_bus(self):
        return self._active_bus

    def set_active_bus(self, bus):
        if bus == self._bus_a:
            self._active_bus = self._bus_a
        elif bus == self._bus_b:
            self._active_bus = self._bus_b
        else:
            raise Exception("bus not valid")

    def switch_bus(self):
        # switch from the current active bus to the other one
        self.stop()
        if self._active_bus == self._bus_a:
            self._active_bus = self._bus_b
        else:
            self._active_bus = self._bus_a
        self._active_bus.restart()
        self.start()

    def start(self):
        self._active = True

    def stop(self):
        self._active = False

    def is_active(self):
        return self._active

    def shutdown(self):
        self._active = False
        self._bus_a.shutdown()
        self._bus_b.shutdown()

    def send(self, frame, timeout=0):
        if self._active:
            self._active_bus.send(frame, timeout)

    def process(self):
        frame = self.frame_buffer.read()
        if frame is None:
            return
        can_id = frame.can_id
        cob_id = can_id & FUNCTION_MASK
        if cob_id == ID_HEARTBEAT:
            if self._heartbeat_handler:
                self._heartbeat_handler(frame)
        elif cob_id == ID_SYNC:
            if self._sync_handler:
                self._sync_handler(frame)
        elif cob_id == ID_SCET:
            if self._scet_handler:
                self._scet_handler(frame)
        elif cob_id == ID_UTC:
            if self._utc_handler:
                self._utc_handler(frame)
        elif cob_id == ID_TC:
            if self._telecommand_handler:
                self._telecommand_handler(frame)
        elif cob_id == ID_TM:
            if self._telemetry_handler:
                self._telemetry_handler(frame)
        elif cob_id == ID_MESSAGE:
            if self._message_handler:
                self._message_handler(frame)

    def register_heartbeat_handler(self, func):
        self._heartbeat_handler = func

    def register_sync_handler(self, func):
        self._sync_handler = func

    def register_scet_handler(self, func):
        self._scet_handler = func

    def register_utc_handler(self, func):
        self._utc_handler = func

    def register_telecommand_handler(self, func):
        self._telecommand_handler = func

    def register_telemetry_handler(self, func):
        self._telemetry_handler = func

    def register_message_handler(self, func):
        self._message_handler = func
