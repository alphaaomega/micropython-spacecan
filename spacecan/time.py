from .frame import Frame
from . import ID_SCET, ID_UTC


PRECISION_8_BITS = 256
PRECISION_16_BITS = (256**2)
PRECISION_24_BITS = (256**3)


class Scet:

    def __init__(self, seconds=0, subseconds=0, precision=PRECISION_24_BITS):
        self.seconds = seconds
        self.subseconds = subseconds
        self.precision = precision

    def __repr__(self):
        return "%s" % (self.seconds + self.subseconds)

    def update_from_frame(self, frame):
        if self.precision == PRECISION_8_BITS:
            fine_time = frame.data[0]
        elif self.precision == PRECISION_16_BITS:
            fine_time =\
                (frame.data[0]) << 8 + frame.data[1]
        elif self.precision == PRECISION_24_BITS:
            fine_time =\
                (frame.data[0] << 16) +\
                (frame.data[1] << 8) +\
                frame.data[2]
        coarse_time =\
            (frame.data[3] << 24) +\
            (frame.data[4] << 16) +\
            (frame.data[5] << 8) +\
            frame.data[6]
        self.seconds = coarse_time
        self.subseconds = fine_time / self.precision


class Utc:

    def __init__(self, day=0, ms_of_day=0, sub_of_ms=0):
        self.day = day
        self.ms_of_day = ms_of_day
        self.sub_of_ms = sub_of_ms

    def __repr__(self):
        return "%s days, %s ms" % (self.day, self.ms_of_day + self.sub_of_ms)

    def update_from_frame(self, frame):
        sub_of_ms = (frame.data[0] << 8) + frame.data[1]
        ms_of_day =\
            (frame.data[2] << 24) +\
            (frame.data[3] << 16) +\
            (frame.data[4] << 8) +\
            frame.data[5]
        day = (frame.data[6] << 8) + frame.data[7]
        self.sub_of_ms = sub_of_ms / PRECISION_16_BITS
        self.ms_of_day = ms_of_day
        self.day = day


class Time:
    """
    The Time service allows sending time frames to allow slave nodes updating
    their local time. The time can be either provides as Spacecraft Elapsed
    Time (SCET) or, optionally as UTC, in a format defined by CCSDS.

    """
    def __init__(self, network):
        self._network = network

    def send(self, time):
        if isinstance(time, Scet):
            coarse_time = time.seconds
            fine_time = int(time.precision * time.subseconds)
            if time.precision == PRECISION_8_BITS:
                fine_time = fine_time << 16
            elif time.precision == PRECISION_16_BITS:
                fine_time = fine_time << 8
            data = bytearray([
                fine_time >> 16,
                (fine_time >> 8) & 0xFF,
                fine_time & 0xFF,
                coarse_time >> 24,
                (coarse_time >> 16) & 0xFF,
                (coarse_time >> 8) & 0xFF,
                coarse_time & 0xFF])
            frame = Frame(
                can_id=ID_SCET + self._network.node_id,
                data=data)
        elif isinstance(time, Utc):
            sub_of_ms = int(time.sub_of_ms * PRECISION_16_BITS)
            ms_of_day = time.ms_of_day
            day = time.day
            data = bytearray([
                sub_of_ms >> 8,
                sub_of_ms & 0xFF,
                ms_of_day >> 24,
                (ms_of_day >> 16) & 0xFF,
                (ms_of_day >> 8) & 0xFF,
                ms_of_day & 0xFF,
                day >> 8,
                day & 0xFF])
            frame = Frame(
                can_id=ID_UTC + self._network.node_id,
                data=data)
        else:
            raise ValueError("not a valid time format")

        try:
            self._network.send(frame)
        except Exception:
            pass  # fail silently
