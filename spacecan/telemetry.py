from .frame import Frame
from . import config, ID_TM


TM_BUFFER_SIZE = config.get('TM_BUFFER_SIZE', 10)


class TelemetryProducer:

    def __init__(self, network):
        self._network = network

    def send(self, data):
        frame = Frame(ID_TM + self._network.node_id, bytearray(data))
        if self._network.is_active():
            try:
                self._network.send(frame)
            except Exception:
                pass  # fail silently


class TelemetryConsumer:

    def __init__(self, network):
        self._network = network
        network.register_telemetry_handler(self._received)
        self._buffer = []

    def _received(self, frame):
        if len(self._buffer) < TM_BUFFER_SIZE:
            self._buffer.append(frame)

    def any(self):
        return len(self._buffer) > 0

    def read(self):
        if not self.any():
            return None
        frame = self._buffer.pop()
        return frame.can_id - ID_TM, frame.data
