from . import logger


MAX_MESSAGE_LENGTH = 4095  # max length of message in bytes


class Message:

    def __init__(self, node_id, data=None):
        if not (0 <= node_id <= 127):
            raise Exception("node id out of allowed range")
        self.node_id = node_id

        if data is None:
            self.data = bytearray()
        else:
            if len(data) > MAX_MESSAGE_LENGTH:
                raise Exception("message exceeds the limit")
            self.data = bytearray(data)

    def __len__(self):
        return len(self.data)


class MessageBuffer:
    """
    A MessageBuffer is used to buffer messages received from the Network.
    The MessageBuffer is designed as a FIFO storage. The MessageBuffer is
    used by the Mailbox class to buffer messages.

    """
    def __init__(self, size):
        self._size = size
        self._data = [[] for _ in range(self._size)]
        self._index_write = 0
        self._index_read = 0

    def write(self, message):
        next_index = (self._index_write + 1) % self._size
        # check if overflow
        if next_index == self._index_read:
            logger.warning("message buffer overflow")
        else:
            self._data[self._index_write] = message
            self._index_write = next_index

    def read(self):
        if self._index_read == self._index_write:
            return None  # buffer empty
        message = self._data[self._index_read]
        self._index_read = (self._index_read + 1) % self._size
        return message

    def any(self):
        if self._index_read == self._index_write:
            return False
        return True

    def clear(self):
        self._index_write = 0
        self._index_read = 0
