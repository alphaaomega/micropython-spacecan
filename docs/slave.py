import logging; logging.get_logger('spacecan').set_level(logging.DEBUG)
import spacecan


MY_NODE_ID = 1


bus_a, bus_b = spacecan.Bus(1), spacecan.Bus(2)
network = spacecan.Network(MY_NODE_ID, bus_a, bus_b)
network.set_filters([
    # receive sync, hb, and telecommands from master
    (spacecan.ID_SYNC, spacecan.FULL_MASK),
    (spacecan.ID_HEARTBEAT, spacecan.FULL_MASK),
    (spacecan.ID_TC + MY_NODE_ID, spacecan.FULL_MASK)])
heartbeat = spacecan.HeartbeatConsumer(network)
sync = spacecan.SyncConsumer(network)
telecommand = spacecan.TelecommandConsumer(network)
telemetry = spacecan.TelemetryProducer(network)

network.start()
heartbeat.start(500)
sync.start()


def main():
    print("Slave node started")

    mode = 1
    counter = 0
    while True:
        # waiting for sync; process any incoming telecommands
        while not sync.is_received():
            # process any incoming telecommands
            while telecommand.any():
                data = telecommand.read()
                mode = data[0]
                print("Mode is now:", mode)
            # do internal housekeeping as needed
            # ...

        # sync received; collect and send telemetry
        if mode == 0:
            print("Sync received.")
        else:
            print('Sync received. Replying telemetry...', end='')
            # meas = pyb.Accel().x()
            telemetry.send([counter])
            counter += 1
            print('done.')


main()  # run the main loop
