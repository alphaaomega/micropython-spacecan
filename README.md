# micropython-spacecan

`spacecan` is a [Micropython](https://micropython.org) implementation of the [SpaceCAN](https://wiki.librecube.org/index.php?title=SpaceCAN) protocol for
embedded systems. The target board is the [pyboard](https://store.micropython.org/product/PYBv1.1H) but it should run on compatible boards as well.

## Getting Started

For a minimal setup you will need two pyboards, which will be used as master
node and slave nodes (although it is possible to replace the slave node with
a CAN bus sniffer device). To extend the network, simply add more slave nodes.

The hardware setup of master and slave nodes is identical. They only differ
in the software setup, as described in the following.

In order for the nodes to communicate, each pyboard must be connected with
a CAN transceiver. The CAN transceivers then interface to the shared CAN bus.
The most simple two-node setup with redundant CAN bus looks like this:

![image](docs/minimal_setup.png)

To run the example application in the **docs** folder, copy the folders
**spacecan** and **lib** to each pyboard. Then copy the files **main.py** and
**spacecan_config.py** to each pyboard. Finally, copy the file **master.py**
and **slave.py** from the **docs** folder to each board. In the **main.py**
file of the master node add the statement ```import master.py```, and
accordingly for the slave node. This will cause the boards to run the file
content upon reboot.
